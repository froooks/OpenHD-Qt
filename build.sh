#!/bin/bash

QT_MAJOR_VERSION=5.15
QT_MINOR_VERSION=7

TYPE=$1

if [ "$TYPE" == "" ]; then
    echo "Missing build type: pi-bullseye, jetson-nano-bionic, radxa-zero-bullseye"
    exit 1
fi

if [ "$TYPE" == "pi-bullseye" ]; then
    QTPLATFORM="eglfs"
    PROCESSES="4"
    PLATFORM="linux-rpi-vc4-g++"
    SSL_ARGS="-openssl"
elif [ "$TYPE" == "jetson-nano-bionic" ]; then
    QTPLATFORM="eglfs"
    PROCESSES="4"
    PLATFORM="linux-jetson-nano-g++"
    SSL_ARGS="-openssl"
elif [ "$TYPE" == "x86-focal" ]; then
    QTPLATFORM="xcb"
    PROCESSES="12"
    PLATFORM="linux-g++"
    SSL_ARGS="-openssl"
elif [ "$TYPE" == "x86-jammy" ]; then
    QTPLATFORM="xcb"
    PROCESSES="12"
    PLATFORM="linux-g++"
    SSL_ARGS="-openssl"
elif [ "$TYPE" == "radxa-zero-bullseye" ]; then
    QTPLATFORM="eglfs"
    PROCESSES="4"
    PLATFORM="linux-radxa-zero-g++"
    SSL_ARGS="-openssl"
fi

PACKAGE_NAME=openhd-qt-${TYPE}

ROOT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
BUILD_DIR=${ROOT}/build

echo "Clean up"
if [ -d "$BUILD_DIR" ]; then rm -Rf "$BUILD_DIR"; fi
mkdir -p "${BUILD_DIR}"

echo "Extracting Qt sources"
tar xf qt-everywhere-opensource-src-${QT_MAJOR_VERSION}.${QT_MINOR_VERSION}.tar.xz -C "${BUILD_DIR}" || exit 1

echo "Configuring Qt for ${TYPE} (${PLATFORM})"
cd "${BUILD_DIR}" || exit
RENDER="opengl desktop"
if [ "$TYPE" == "pi-bullseye" ]; then
    git clone https://github.com/OpenHD/qt-raspberrypi-configuration.git
    RENDER="opengl es2"
elif [ "$TYPE" == "jetson-nano" ]; then
    git clone https://github.com/OpenHD/qt-raspberrypi-configuration.git
    RENDER="opengl es2"
elif [ "$TYPE" == "radxa-zero-bullseye" ]; then
    git clone https://gitlab.com/froooks/qt-raspberrypi-configuration.git
    RENDER="opengl es2"
fi

cd "${BUILD_DIR}"/qt-raspberrypi-configuration || exit
make install DESTDIR="${BUILD_DIR}"/qt-everywhere-src-${QT_MAJOR_VERSION}.${QT_MINOR_VERSION}

mkdir -p "${BUILD_DIR}"/qt-build
cd "${BUILD_DIR}"/qt-build || exit

echo "Configuring Qt for ${TYPE} (${PLATFORM})"
../qt-everywhere-src-${QT_MAJOR_VERSION}.${QT_MINOR_VERSION}/configure -platform ${PLATFORM} \
-${RENDER} \
-silent \
-opensource -confirm-license -release \
-reduce-exports \
-force-pkg-config \
-nomake examples -no-compile-examples -nomake tests \
-skip qtwebengine \
-skip qtxmlpatterns \
-skip qtsensors \
-skip qtpurchasing \
-skip qtnetworkauth \
-skip qt3d \
-no-feature-cups \
-no-feature-testlib \
-no-feature-dbus \
-no-feature-vnc \
-no-compile-examples \
-no-feature-geoservices_mapboxgl \
-qt-pcre \
-no-pch \
-ssl \
-kms \
-xcb \
-xcb-xlib \
-gtk \
-gbm \
-evdev \
-eglfs \
-system-freetype \
-fontconfig \
-glib \
-prefix /opt/Qt${QT_MAJOR_VERSION}.${QT_MINOR_VERSION} \
-no-feature-eglfs_brcm \
-qpa ${QTPLATFORM} || exit 1

cd "${BUILD_DIR}"/qt-build || exit
ls -a
sed -i '309 i #elif defined(__ARM_ARCH_8A__)' ../qt-everywhere-src-${QT_MAJOR_VERSION}.${QT_MINOR_VERSION}/qtscript/src/3rdparty/javascriptcore/JavaScriptCore/wtf/Platform.h
sed -i '310 i #define WTF_CPU_ARM_TRADITIONAL 1' ../qt-everywhere-src-${QT_MAJOR_VERSION}.${QT_MINOR_VERSION}/qtscript/src/3rdparty/javascriptcore/JavaScriptCore/wtf/Platform.h

echo "Building Qt for ${TYPE} (${PLATFORM})"

make -j${PROCESSES}

echo "Build Complete"
TMPDIR=${BUILD_DIR}/${PACKAGE_NAME}
mkdir -p ${TMPDIR}

INSTALL_ROOT=${TMPDIR} make install

echo "Install Complete"
